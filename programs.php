<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>programs.php</title>
</head>
<body>

<h2>Etape 2</h2>
Après réflexion, l'administration de l'école aimerait aussi pouvoir gérer les salles de l'école.
Il faudrait donc pouvoir lister les différentes salles (A1 jusqu'à A10, B1 jusqu'à B6) et les cours qui y ont lieu pour chaque horaire.

Cas d'utilisation

<p>En tant qu'administrateur, je veux savoir (avec l'id d'une salle) quels sont les cours à venir dans cette salle et sur quels horaires.</p>
<p>En tant qu'administrateur, je veux savoir (avec l'id d'une salle) quels sont les cours à venir dans cette salle et sur quels horaires.</p>
<p>/!\ 1, 3, 10, 15 --> seul ces ID sont assignée.</p>
<?php

include 'bdd.php';

// 1, 3, 10, 15  -> seul ces ID sont dispo.

$id = $_GET['id'];

echo "<p>Renvoie les informations de l'étudiant à partir de son id (les parents ont bien l'id de leur enfant).</p>";
$query = $pdo->query("SELECT * FROM classrooms 
INNER JOIN courses ON classrooms.idclassrooms = courses.classrooms_idclassrooms
INNER JOIN professors ON professors.idprofessors = courses.professors_idprofessors
WHERE classrooms.idclassrooms = $id");
$resultat = $query->fetchAll();
// var_dump($id);
// var_dump($resultat);
?>
<table border="1">

<?PHP

    echo "<tr>
    <td>idclassrooms</td>
    <td>name</td>
    <td>date</td>
    <td>time</td>
    <td>idcourses</td>
    <td>entilted</td>
    <td>duration</td>
    <td>idprofessors</td>
    <td>Nom</td>
    <td>Prénom</td>
    <td>email</td>
    <td>status</td>
    </tr>";

    foreach ($resultat as $key => $value) {
        echo "<tr>";
        echo "<td>" . $resultat[$key]['idclassrooms'] . "</td>";
        echo "<td>" . $resultat[$key]['name'] . "</td>";
        echo "<td>" . $resultat[$key]['date'] . "</td>";
        echo "<td>" . $resultat[$key]['time'] . "</td>";
        echo "<td>" . $resultat[$key]['idcourses'] . "</td>";
        echo "<td>" . $resultat[$key]['entilted'] . "</td>";
        echo "<td>" . $resultat[$key]['duration'] . "</td>";
        echo "<td>" . $resultat[$key]['idprofessors'] . "</td>";
        echo "<td>" . $resultat[$key]['last_name'] . "</td>";
        echo "<td>" . $resultat[$key]['first_name'] . "</td>";
        echo "<td>" . $resultat[$key]['email'] . "</td>";
        echo "<td>" . $resultat[$key]['status'] . "</td>";
    }

?>
</table>

</body>
</html>