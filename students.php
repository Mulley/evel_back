<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>students.php</title>
</head>
<body>
<h2>Etape 1</h2>
<p>L'API doit pouvoir permettre de lister les infos d'un élève (nom, prénom, adresse du domicile, date de naissance, date d'inscription), d'un programme (intitulé, cours inclus dedans), d'un cours (intitulé du cours, durée, professeur en charge du cours) ou d'un professeur (nom, prénom, adresse mail, est-il prestataire ou salarié?).</p>

<p>Cas d'utilisation :</p>

<p>En tant que parent, je veux consulter les infos de mon enfant à partir de son ID.</p>
<p>En tant que parent, je veux savoir (à partir de l'ID de mon enfant) le programme auquel il est inscrit et les cours qui sont inclus dedans (ainsi que les professeurs).</p>
<p>En tant qu'élève, je veux consulter mon emploi du temps (à partir de mon id) : quels cours, dans quelle salle, à quelle heure, avec quel professeur.</p>
<?php
include 'bdd.php';

$id = $_GET['id'];

echo "<p>Renvoie les informations de l'étudiant à partir de son id (les parents ont bien l'id de leur enfant).</p>";
$query = $pdo->query("SELECT * FROM students WHERE ideleves = $id");
$resultat = $query->fetchAll();
// var_dump($id);

?>
<table border="1">

<?PHP

    echo "<tr>
    <td>ID elev</td>
    <td>last_name</td>
    <td>first_name</td>
    <td>address</td>
    <td>postal_code</td>
    <td>city</td>
    <td>birth_date</td>
    <td>registration_date</td>
    <td>programs_idprograms</td>
    <td>programme</td>
    <td>matière</td>
    <td>prof</td>
    <td>salle</td>
    <td>date du cours</td>
    <td>durée</td>
    </tr>";

    foreach ($resultat as $key => $value) {
        echo "<tr>";
        echo "<td>" . $resultat[$key]['ideleves'] . "</td>";
        echo "<td>" . $resultat[$key]['last_name'] . "</td>";
        echo "<td>" . $resultat[$key]['first_name'] . "</td>";
        echo "<td>" . $resultat[$key]['address'] . "</td>";
        echo "<td>" . $resultat[$key]['postal_code'] . "</td>";
        echo "<td>" . $resultat[$key]['city'] . "</td>";
        echo "<td>" . $resultat[$key]['birth_date'] . "</td>";
        echo "<td>" . $resultat[$key]['registration_date'] . "</td>";
        echo "<td>" . $resultat[$key]['programs_idprograms'] . "</td>";

        $id_p = $resultat[$key]['programs_idprograms'];
        $recup_prog = $pdo->query("SELECT programs.entitled AS programme, courses.entilted AS cours, `last_name`, `name`, `date`, `time` FROM courses_has_programs 
        INNER JOIN programs ON courses_has_programs.programs_idprograms = programs.idprograms
        INNER JOIN courses ON courses_has_programs.courses_idcourses = courses.idcourses
        INNER JOIN classrooms ON courses_has_programs.courses_classrooms_idclassrooms = classrooms.idclassrooms
        INNER JOIN professors ON courses_has_programs.courses_professors_idprofessors = professors.idprofessors
        WHERE programs.idprograms = ($id_p)");
        $resultat_prog = $recup_prog->fetchAll();
        // var_dump($resultat_prog);
        // echo json_encode($resultat_prog);

        echo "<td>" .$resultat_prog[0]['programme'].  "</td>";
        echo "<td>" .$resultat_prog[0]['cours'].  "</td>";
        echo "<td>" .$resultat_prog[0]['last_name'].  "</td>";
        echo "<td>" .$resultat_prog[0]['name'].  "</td>";
        echo "<td>" .$resultat_prog[0]['date'].  "</td>";
        echo "<td>" .$resultat_prog[0]['time'].  "</td>";

        echo "</tr>";
    }

?>
</table>
</body>
</html>