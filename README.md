# bienvenus sur l'API de Poudlard !
## Comment m'utiliser
* Ayez une enfant dans notre école
* Clonez ce repos
* Créez votre BDD ( nom suggére: "poudlard")
* Changez le MDP dans le fichier bdd.php
* Lancez la dump pour alimenter votre BDD ( As you want, PhpMyAdmin ou autre ...)
* Lancez votre server avec la commande : php -S localhost:8000 [ou le port de votre choix]
* Puis accédez a l'url : "http://localhost:8000/students.php?id=1"
* Remplacez l'ID par celui de votre enfant, c'est mieux...

## Si vous étes de l'administration :
* http://localhost:8000/programs.php?id=10
-> l'id est aussi à remplacer (/!\ 1, 3, 10, 15 --> seul ces ID sont assignée.)


** Post-scriptum : Désolé, JSON est absent de la liste **