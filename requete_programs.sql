SELECT programs.entitled AS programme, courses.entilted AS cours, `last_name`, `name`, `date`, `time` FROM courses_has_programs 
INNER JOIN programs ON courses_has_programs.programs_idprograms = programs.idprograms
INNER JOIN courses ON courses_has_programs.courses_idcourses = courses.idcourses
INNER JOIN classrooms ON courses_has_programs.courses_classrooms_idclassrooms = classrooms.idclassrooms
INNER JOIN professors ON courses_has_programs.courses_professors_idprofessors = professors.idprofessors
WHERE programs.idprograms = 3;